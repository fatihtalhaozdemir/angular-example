angular
    .module('app', [])
    .controller('musteritakip', ['$scope', '$filter', 'User', function($scope, $filter, User) {

        $scope.repeatKeys = ['musteri_adi', 'musteri_soyadi', 'musteri_telefonu', 'musteri_eposta', 'musteri_adresi'];
        
        getUsers();

        function searchUser(query) {
            $scope.shownClientList = $filter('filter')($scope.clientList, query);
        }
        $scope.searchUser = searchUser;

        function getUsers() {
            User
                .index()
                .success(function(response) {
                    console.log(response.message);
                    $scope.shownClientList = response.data;
                    $scope.clientList = response.data;
                });
        }

        function insertUser(body) {
            console.log(body);
            User
                .store(body)
                .success(function(response) {
                    console.log(response.message);
                    getUsers();
                });
        }
        $scope.insertUser = insertUser;

    }])
    .service('User', ['$http', function($http) {
        return {
            index: function(params) {
                return $http.get("users.php?action=index", params);
            },
            store: function(body, params) {
                return $http.post("users.php?action=store", body, params);
            }
        };
    }]);
