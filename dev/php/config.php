<?php

require "basicDB.php";

$dbUser = ($_SERVER['SERVER_NAME'] == '127.0.0.1' || $_SERVER['SERVER_NAME'] == 'localhost') ? 'root' : 'root';

$dataBase = new BasicDB('localhost', 'angular-example', $dbUser, 'root');

if(! $dataBase){
	throw new Exception("Bağlantı başarısız", 500);
}

function getRequestData()
{
	return (object) json_decode(file_get_contents("php://input"));
}

function responseJson($data, $status = 200)
{
	header("Content-type: application/json; charset=utf-8");

	http_response_code($status);
	echo json_encode($data);
}