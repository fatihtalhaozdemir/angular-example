<?php

require_once 'config.php';

switch ($_GET['action']) {
	case 'index':
		usersIndex();
		break;

	case 'store':
		usersStore();
		break;
	
	default:
		usersIndex();
		break;
}

function usersIndex()
{
	global $dataBase;

	try {
		$users = $dataBase->select('musteriler')->run();

		$response = [
			'message' => 'Kullanıcılar listelendi',
			'data' => $users,
		];

		responseJson($response);
	} catch (Exception $e) {

		$response = [
			'message' => $e->getMessage()
		];

		responseJson($response, 500);
	}
}

function usersStore()
{
	global $dataBase;

	$data = getRequestData();

	try {
		$insert = $dataBase->insert('musteriler')->set((array) $data);

		$response = [
			'message' => 'Ekleme başarılı',
		];

		responseJson($response);
	} catch (Exception $e) {

		$response = [
			'message' => 'Müşteri eklenemedi'
		];

		responseJson($response, 500);
	}
}

?>