'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var pug = require('gulp-pug');
//var minify = require('gulp-minifier');
var watch = require('gulp-watch');


gulp.task('pugs', function buildHTML() {
  return gulp.src('./dev/pug/*.pug')
  .pipe(pug({
    pretty: true
  }))
  .pipe(gulp.dest('./build'))
  .pipe(pug().on('error', function(err) {
    console.log(err)
  }));
});


gulp.task('sass', function () {
  return gulp.src('./dev/sass/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./build/assets/css/'));
});


gulp.task('icons', function() { 
    return gulp.src('bower_components/font-awesome/fonts/**.*')
    .pipe(gulp.dest('./build/assets/fonts'));
});

/*gulp.task('minifier', function() {
  return gulp.src('./www/assets/css/*.css').pipe(minify({
    minify: true,
    collapseWhitespace: true,
    conservativeCollapse: true,
    minifyJS: true,
    minifyCSS: true,
    getKeptComment: function (content, filePath) {
        var m = content.match(/\/\*![\s\S]*?\*\//img);
        return m && m.join('\n') + '\n' || '';
    }
  })).pipe(gulp.dest('./www/assets/css/'));
});*/

/*gulp.task('jquery', function() {
  return gulp.src(['./bower_components/jquery/dist/jquery.min.js'])
    .pipe(concat('jquery.min.js'))
    .pipe(gulp.dest('./build/assets/js'));
});*/

/*gulp.task('scripts', function() {
  return gulp.src(['./bower_components/bootstrap-sass/assets/javascripts/bootstrap.min.js'])
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./build/assets/js'));
});*/

gulp.task('php', function() {
    return gulp.src('./dev/php/*.php')
    .pipe(gulp.dest('./build/'));
});

gulp.task('angular', function() {
    return gulp.src('./node_modules/angular/angular.min.js')
    .pipe(gulp.dest('./build/assets/js/'));
});

gulp.task('controller', function() {
    return gulp.src('./dev/js/controller.js')
    .pipe(gulp.dest('./build/assets/js/'));
});




function whatcher(paths, tasks) {
  watch(paths, function(){ 
  gulp.start(tasks);
  });
}


gulp.task('watch', function() {
  whatcher(['dev/pug/**/*.pug'], ['pugs']);
  whatcher(['dev/sass/**/*.scss'], ['sass']);
  whatcher(['dev/php/*.php'], ['php']);
  whatcher(['dev/js/**/*.js'], ['controller']);
});




gulp.task('default', function(){
  gulp.start('pugs', 'sass' , 'icons', 'angular', 'php' , /*'scripts', 'jquery' ,*/ 'watch');
});