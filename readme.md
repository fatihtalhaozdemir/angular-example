#### Restful API ile çalışan AngularJS

PDO ile mysql e bağlantı sağlayan mini restful api ile çalışan angular js ile ekle,sil,çıkar işlemleri yapabilirsiniz.
<hr>

##### Ön Bilgi
Proje dosyaları gulp ile derlenerek oluşturuluyor. Derleme araçlarını kurduğunuzda dev altındaki bulunan ; <br/>
 `pug` dosyaları `html`, <br/>
 `sass`  dosyaları `css`, <br/>
 `javascript`  dosyaları `javascript`, <br/>
 `php`  dosyaları `php`, <br/>
 olarak derlenmiş halde çıkartacaktır.


#### Derleme araçlarının kurulması
Komut satırından sırasıyla <br/>
`npm install` <br/>
`npm run watch` <br/>
komutlarını çalıştırın.

#### Database Bağlantısı

Username: `root` <br>
Password: `root` <br>
Database Adı: `angular-example` <br>

####Database Yapısı

Oluşturulması Gereken Tablo Adı: `musteriler`<br>
Oluşturulması Gereken Kolonlar:<br>
`musteri_adi`<br>
`musteri_adresi`<br>
`musteri_eposta`<br>
`musteri_soyadi`<br>
`musteri_telefonu`<br>